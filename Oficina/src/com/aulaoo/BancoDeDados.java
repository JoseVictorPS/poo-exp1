package com.aulaoo;

import java.util.ArrayList;

public class BancoDeDados {
    ArrayList<Atendimento> atendimentos;
    ArrayList<Cliente> clientes;
    ArrayList<Veiculo> veiculos;

    public BancoDeDados(){
        atendimentos = new ArrayList<>();
        clientes = new ArrayList<>();
        veiculos = new ArrayList<>();
    }

    public void insert(Atendimento a){
        atendimentos.add(a);
    }
    public void insert(Cliente c) {
        clientes.add(c);
    }
    public void insert(Veiculo v) {
        veiculos.add(v);
    }
    public void remove(Atendimento a){
        atendimentos.remove(a);
    }
    public void remove(Cliente c) {
        clientes.remove(c);
    }
    public void remove(Veiculo v) {
        veiculos.remove(v);
    }

    public Atendimento searchApointment(String plaque) throws NotFoundInBdException{
        for(Atendimento a : atendimentos){
            if(a.getVehicle().getPlaque().equals(plaque)) return a;
        }
        throw new NotFoundInBdException();
    }

    public Cliente searchClient(String nome) throws NotFoundInBdException {
        for(Cliente c : clientes){
            if(c.getName().equals(nome)) return c;
        }
        throw new NotFoundInBdException();
    }

    public Veiculo searchVehicle(String plaque) throws NotFoundInBdException {
        for(Veiculo v : veiculos) {
            if(v.getPlaque().equals(plaque)) return v;
        }
        throw new NotFoundInBdException();
    }
}
