package com.aulaoo;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Atendimento {
    private Cliente c;
    private String d;
    private Veiculo v;
    private String prob;
    private String res;
    private boolean aberto;

    public Atendimento(Cliente cc, Veiculo vv, String p) {
        c = cc;
        v= vv;
        d = setData();
        aberto = true;
        prob = p;
    }

    private String setData() {
        Date data = new Date();
        Format formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.format(data);
    }

    public boolean isAberto() {
        return aberto;
    }

    public void setRes(String s) {
        if(isAberto()) {
            res = s;
            aberto = false;
            return;
        }
        System.out.println("Já resolvido!");
    }

    public Veiculo getVehicle(){
        return v;
    }

    public void show(){
        System.out.print("O cliente: "+c.getName()
        + "\nO veiculo: "+v.getPlaque()
        + "\nNo dia: "+d
        + "\nProblema: "+prob);
        if(!isAberto()) System.out.println("\nResolucao: "+res);
        else System.out.println();
    }

    public void edit(Cliente cc) {
        c = cc;
    }
    public void edit(Veiculo vv){
        v = vv;
    }
    public void edit(String s) {
        if(isAberto()) prob = s;
        else res = s;
    }
}
