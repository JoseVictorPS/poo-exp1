package com.aulaoo;

public class Veiculo {
    private String modelo;
    private String placa;

    public Veiculo(String mod, String p) {
        modelo = mod;
        placa = p;
    }

    public String getPlaque(){
        return placa;
    }
    public void show() {
        System.out.println("Modelo: "+modelo+"\nPlaca: "+placa);
    }
    public void editModel(String mod){
        modelo = mod;
    }
    public void editPlaque(String p) {
        placa = p;
    }
}
