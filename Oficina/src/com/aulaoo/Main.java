package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        BancoDeDados bd = new BancoDeDados();
        Veiculo v1 = new Veiculo("Fiat Uno","1234ABC");
        bd.insert(v1);
        Cliente mont = new Cliente("Montadora Foster");
        bd.insert(mont);
        mont.insert(v1);
        Atendimento att = new Atendimento(mont, v1, "Troca de óleo");
        bd.insert(att);
        att.show();
        try{
            bd.searchApointment("1234ABC").getVehicle().editPlaque("ABC1234");
            bd.searchApointment("4566");
            bd.searchApointment("ABC1234").getVehicle().editPlaque("AAA666");
        } catch (NotFoundInBdException e) {
            //System.out.println(e);
            System.out.println("Não achado!!!");
        }
        v1.show();
    }
}
