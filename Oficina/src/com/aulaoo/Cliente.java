package com.aulaoo;

import java.util.ArrayList;

public class Cliente {
    private String nome;
    private ArrayList<Veiculo>veiculos;

    public Cliente(String n) {
        nome = n;
        veiculos = new ArrayList<>();
    }
    public String getName(){
        return nome;
    }
    public void show(){
        System.out.println("Nome: "+nome);
        for(Veiculo v : veiculos)v.show();
    }
    public void insert(Veiculo v) {
        veiculos.add(v);
    }
    public void edit(String n){
        nome = n;
    }
    public void remove(Veiculo v) {
        veiculos.remove(v);
    }
    public Veiculo searchVehicle(String plaque) throws NotFoundInBdException{
        for(Veiculo v : veiculos) {
            if(v.getPlaque().equals(plaque)) return v;
        }
        throw new NotFoundInBdException();
    }
}
