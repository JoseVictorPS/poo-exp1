package com.aulaoo;

public interface Relogio {
    public void setHorario(int[]horario);
    public int[] getHorario();
    public void setHorarioAlarme(int[]horario);
    public int[] getHorarioAlarme();
    public void ligarAlarme();
    public void desligarAlarme();
    public void setVolumeRelogio(int volume);
    public int getVolumeRelogio();
}
