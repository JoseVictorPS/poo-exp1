package com.aulaoo;

public class RadioRelogio implements Radio, Relogio{
    private float emissora;
    private String tipoEmi;
    private int volumeRadio;
    private int[]horario;
    private boolean alarmeStatus;
    private int[]alarmeHora;
    private int volumeRelogio;

    public RadioRelogio() {
        horario = new int[2];
        alarmeHora = new int[2];
    }

    @Override
    public void setEmissora(float emissora, String tipoEmi) {
        this.emissora = emissora;
        this.tipoEmi = tipoEmi;
    }

    @Override
    public float getEmissora() {
        return this.emissora;
    }

    @Override
    public String getTipoEmissora() {
        return this.tipoEmi;
    }

    @Override
    public void setVolumeRadio(int v) {
        this.volumeRadio = v;
    }

    @Override
    public int getVolumeRadio() {
        return this.volumeRadio;
    }

    @Override
    public void setHorario(int[] horario) {
        this.horario[0] = horario[0];
        this.horario[1] = horario[1];
    }

    @Override
    public int[] getHorario() {
        return this.horario;
    }

    @Override
    public void setHorarioAlarme(int[] horario) {
        this.alarmeHora[0] = horario[0];
        this.alarmeHora[1] = horario[1];
    }

    @Override
    public int[] getHorarioAlarme() {
        return this.alarmeHora;
    }

    @Override
    public void ligarAlarme() {
        alarmeStatus = true;
    }

    @Override
    public void desligarAlarme() {
        alarmeStatus = false;
    }

    @Override
    public void setVolumeRelogio(int volume) {
        this.volumeRelogio = volume;
    }

    @Override
    public int getVolumeRelogio() {
        return this.volumeRelogio;
    }
}
