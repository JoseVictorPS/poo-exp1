package com.aulaoo;

public interface Radio {
    public void setEmissora(float emissora,String tipoEmi);
    public float getEmissora();
    public String getTipoEmissora();
    public void setVolumeRadio(int v);
    public int getVolumeRadio();
}
