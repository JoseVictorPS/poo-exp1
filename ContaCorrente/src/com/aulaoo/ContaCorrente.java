package com.aulaoo;

public class ContaCorrente {
    private float saldo;

    public ContaCorrente(){
        saldo = 0;
    }

    public void depositar(float q){
        saldo += q;
    }

    public float sacar(float q)throws SaqueNegativoException,
            SaldoInsuficienteException, ValorSuspeitoException{
        if (q * 1.005f > saldo) {
            throw new SaldoInsuficienteException();
        }
        if(q < 0) {
            throw new SaqueNegativoException();
        }
        saldo -= q * 1.005f;
        return q;
    }

    public void saldo() {
        System.out.println("O saldo é: "+saldo);
    }
    protected float getSaldo(){
        return saldo;
    }
    protected void setSaldo(float s){
        saldo = s;
    }
}
