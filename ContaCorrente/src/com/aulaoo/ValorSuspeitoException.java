package com.aulaoo;

public class ValorSuspeitoException extends Exception{
    private float valor;

    public ValorSuspeitoException(float v) {
        super();
        this.valor = v;
    }

    public String toString() {
        return "Valor suspeito " + valor + ". Saque impedido!";
    }
}
