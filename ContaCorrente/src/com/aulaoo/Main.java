package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente();
        cc.depositar(1000.0f);
        try {
            cc.sacar(500.0f);
            cc.sacar(5000.0f);
        } catch (SaqueNegativoException e) {
            System.out.println("Saque de número negativo!");
        } catch (SaldoInsuficienteException e) {
            System.out.println("Saldo insuficiente!");
        } catch (ValorSuspeitoException e) {}
        cc.saldo();
        ContaEspecial ce = new ContaEspecial();
        ce.depositar(3000.0f);
        try{
            ce.sacar(2100.0f);
        } catch (SaqueNegativoException e) {
            System.out.println("Saque de número negativo!");
        } catch (SaldoInsuficienteException e) {
            System.out.println("Saldo insuficiente!");
        } catch (ValorSuspeitoException e) {
            System.out.println(e);
        }
        ce.saldo();
        try {
            ce.sacar(-100.0f);
        } catch (SaqueNegativoException e) {
            System.out.println("Saque de número negativo!");
        } catch (SaldoInsuficienteException e) {
            System.out.println("Saldo insuficiente!");
        } catch (ValorSuspeitoException e) {
            System.out.println(e);
        }
        ce.saldo();
    }
}
