package com.aulaoo;

public class ContaEspecial extends ContaCorrente{
    float limite = 2000.0f;

    public float sacar(float q)throws SaldoInsuficienteException,
            SaqueNegativoException, ValorSuspeitoException{
        float n = q*1.001f;
        if (n > getSaldo()) {
            throw new SaldoInsuficienteException();
        }
        if(n > limite) {
            throw new ValorSuspeitoException(q);
        }
        if(q<0) {
            throw new SaqueNegativoException();
        }
        setSaldo(getSaldo()-n);
        return q;
    }

}
