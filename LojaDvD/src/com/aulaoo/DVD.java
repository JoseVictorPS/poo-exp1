package com.aulaoo;

public class DVD extends Produto{
    private int duracao;

    public DVD(int c, String n, float p, int d) {
        super(c, n, p);
        duracao = d;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int d) {
        duracao = d;
    }

    public String toString() {
        return super.toString()+
                "\nDuração em minutos: "+duracao;
    }

}
