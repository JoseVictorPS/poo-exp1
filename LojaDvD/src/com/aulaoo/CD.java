package com.aulaoo;

public class CD extends Produto{
    private int NUM_FAIXA;

    public CD(int c, String n, float p, int num) {
        super(c, n, p);
        NUM_FAIXA = num;
    }

    public int getNUM_FAIXA(){
        return NUM_FAIXA;
    }

    public void setNUM_FAIXA(int n) {
        NUM_FAIXA = n;
    }

    public String toString() {
        return super.toString() +
                "\nNúmero de faixas: "+NUM_FAIXA;
    }

}
