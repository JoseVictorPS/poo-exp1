package com.aulaoo;

public class Produto {
    private String nome;
    private float preco;
    private int codigo;

    public String toString(){
         return "Nome: "+getNome()+
                "\nPreco: "+getPreco();
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String n) {
        nome = n;
    }
    public float getPreco() {
        return preco;
    }
    public void setPreco(float p) {
        preco = p;
    }
    public Produto(int cod, String n, float p) {
        this.nome = n;
        this.preco = p;
        this.codigo = cod;
    }
    public int getCodigo() {
        return codigo;
    }
    public boolean equals(Produto p) {
        if(codigo == p.getCodigo()) return true;
        return false;
    }
}
