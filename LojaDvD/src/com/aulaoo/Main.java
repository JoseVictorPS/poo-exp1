package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        Produto[] produtos = new Produto[5];
        produtos[0] = new DVD(123456,
                "American Pie 2",
                19.99f, 122);
        produtos[1] = new Livro(654321,
                "50 tons de lixo",
                0.99f, "Um maluco aí");
        produtos[2] = new CD(333222,
                "Good Blood HeadBangers",
                6.66f, 10);
        produtos[3] = new DVD(654321,
                "TenaciusD: uma dupla infernal",
                20.99f, 114);
        produtos[4] = new Livro(444555,
                "Mindhunter",
                34.99f, "Jhon E. Douglas e Mark Olshaker");
        for(Produto p : produtos)
            System.out.println(p.toString());
        Busca(produtos[3], produtos);
    }

    private static void Busca(Produto p, Produto[]v) {
        for(int i=0; i<v.length; i++) {
            if(p.equals(v[i])) {
                System.out.println("Está na posição " + i);
                return;
            }
        }
        System.out.println("Não encontrado!");
    }
}
