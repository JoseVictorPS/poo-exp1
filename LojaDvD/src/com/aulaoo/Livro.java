package com.aulaoo;

public class Livro extends Produto{
    private String autor;

    public Livro(int c, String n, float p, String a) {
        super(c, n, p);
        autor = a;
    }

    public void setAutor(String a) {
        autor = a;
    }

    public String toString() {
        return super.toString()+
                "\nAutor: "+autor;
    }
}
