package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        Figuras[] figuras = new Figuras[3];
        figuras[0] = new Retangulo(5.5f, 10.0f);
        figuras[1] = new Quadrado(6.0f);
        figuras[2] = new Circulo(3.0f);
        for(Figuras f : figuras)f.show(f);
    }
}
