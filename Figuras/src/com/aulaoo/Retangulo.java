package com.aulaoo;

public class Retangulo extends Quadrilateros{
    public Retangulo(float l1, float l2) {
        super(l1, l2, l2, l1);
    }

    @Override
    public float Area() {
        return getLados()[0]*getLados()[1];
    }
}
