package com.aulaoo;

public interface Figuras {
    public float Perimetro();
    public float Area();
    public void show(Figuras q);
}
