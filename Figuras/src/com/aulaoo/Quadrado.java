package com.aulaoo;

public class Quadrado extends Quadrilateros{
    public Quadrado(float l) {
        super(l, l, l, l);
    }

    @Override
    public float Area() {
        return getLados()[0]*getLados()[1];
    }

}
