package com.aulaoo;

public abstract class Quadrilateros implements Figuras{
    private float[] lados;

    public Quadrilateros(float l1, float l2,
                         float l3, float l4)
    {
        lados = new float[4];
        lados[0]=l1;
        lados[1]=l2;
        lados[2]=l3;
        lados[3]=l4;
    }
    public float Perimetro(){
        float p=0;
        for(float l : lados) p+=l;
        return p;
    }
    public abstract float Area();
    protected float[] getLados() {return lados;}
    public void show(Figuras q){
        System.out.print("O(s) lado(s) é(são): ");
        if(q instanceof Retangulo)System.out.println(lados[0]+" e "+lados[1]);
        if(q instanceof Quadrado) System.out.println(lados[0]);
        System.out.println("Perimetro: "+Perimetro()
                +"\nArea: "+Area());
    }
}
