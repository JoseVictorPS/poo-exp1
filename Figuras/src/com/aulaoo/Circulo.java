package com.aulaoo;

public class Circulo implements Figuras{
    private float radius;
    public Circulo(float r) {
        radius = r;
    }

    @Override
    public float Area() {
        return (float)(Math.PI*Math.pow(radius, 2));
    }

    @Override
    public float Perimetro() {
        return (float)(2*Math.PI*radius);
    }

    public void show(Figuras q) {
        System.out.println("Circulo:" +
                "\nRaio: " + radius +
                "\nPerimetro: "+Perimetro()
                +"\nArea: "+Area());
    }
}
