package com.aulaoo;

public final class Utils {
    public static double toCelsius(double f) throws FarenheitException{
        if (f < -459.67) {
            throw new FarenheitException();
        }
        return (5 / 9) * (f - 32);
    }
}
