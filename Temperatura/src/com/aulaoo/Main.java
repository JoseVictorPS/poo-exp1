package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        try {
            double c = Utils.toCelsius(-500.0f);
            System.out.println(c);
        } catch (TemperatureException e) {
            System.out.println("Erro 1");
        }
        /*catch (FarenheitException e) {
            System.out.println("Erro 2");
        }*/
        /*Ou a exceção é pega no primeiro catch, onde a exceção pai
        * é percebido pelo throw do toCelsius, ou ela é pega no
        * segundo catch, onde a própria exceção filha é pega.
        * Tentar pegar as duas, seria redundante. É como um
        * downcasting/upcasting de classes pais e filhas */

    }
}
